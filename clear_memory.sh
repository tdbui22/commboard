#!/bin/bash -e

cd "${BASH_SOURCE%/*}" || exit

bin/console doctrine:cache:clear-query
bin/console doctrine:cache:clear-metadata
bin/console doctrine:cache:clear-result
bin/console cache:pool:clear cache.app
