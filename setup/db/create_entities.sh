#!/bin/bash

cd "${BASH_SOURCE%/*}" || exit

if [ -d ../../tmp ]
then
rm -r ../../tmp
fi

php ../../bin/console doctrine:mapping:convert --force --from-database --namespace='App\Entity\' annotation ../../tmp
