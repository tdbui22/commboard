#!/bin/bash -e

cd "${BASH_SOURCE%/*}" || exit

DELTA=$1

IFS=';' read -ra DDL <<< `cat deltas/$DELTA`
for SQL in "${DDL[@]}"; do
    php ../../bin/console doctrine:query:sql "$SQL"
done

php ../../bin/console doctrine:cache:clear-metadata
php ../../bin/console doctrine:cache:clear-query
php ../../bin/console doctrine:cache:clear-result
