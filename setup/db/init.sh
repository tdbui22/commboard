#!/bin/bash -e

cd "${BASH_SOURCE%/*}" || exit

php ../../bin/console doctrine:database:drop --force
php ../../bin/console doctrine:database:create

IFS=';' read -ra DDL <<< `cat init/ddl.sql`
for SQL in "${DDL[@]}"; do
    php ../../bin/console doctrine:query:sql "$SQL" > /dev/null
done

for DELTA in `ls deltas`; do
    echo $DELTA;

    IFS=';' read -ra DDL <<< `cat deltas/$DELTA`
    for SQL in "${DDL[@]}"; do
        php ../../bin/console doctrine:query:sql "$SQL" > /dev/null
    done
done

if [ "$APP_ENV" != "prod" ]; then
    echo 'Running dev queries'

    IFS=';' read -ra DDL <<< `cat init/dev.sql`
    for SQL in "${DDL[@]}"; do
        php ../../bin/console doctrine:query:sql "$SQL" > /dev/null
    done
fi

../../clear_memory.sh
../../clear_cache.sh
