<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Client;

class WebTest extends AppTest
{
    /** @var Client */
    protected $client;

    protected function setUp()
    {
        $this->client = $this->createClient();
    }

    protected function createClient(array $options = array(), array $server = array())
    {
        $client = $this->getContainer($options)->get('test.client');
        $client->setServerParameters($server);

        return $client;
    }
}
