<?php

namespace App\Tests;

use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Driver\Statement;
use Doctrine\ORM\Configuration;
use Doctrine\Common\Cache\Cache;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Query\Expr;
use Symfony\Bundle\FrameworkBundle\Console\Application;

abstract class AppTest extends KernelTestCase
{
    /** @var Application */
    private $application;

    /**
     * @param array $options
     * @return \Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected function getContainer(array $options = [])
    {
        if (!self::$container) {
            static::bootKernel($options);
            self::$container = static::$kernel->getContainer();
        }

        return self::$container;
    }

    protected function get($id)
    {
        return self::getContainer()->get($id);
    }

    protected function getTest($id)
    {
        return self::getContainer()->get('test.' . $id);
    }

    protected function getCsrfToken($name)
    {
        return self::getContainer()->get('security.csrf.token_manager')->getToken($name);
    }

    /**
     * @param string $parameter
     * @return mixed
     */
    protected function getParameter($parameter)
    {
        return self::getContainer()->getParameter($parameter);
    }

    /**
     * @param null $statement
     * @return Registry
     */
    protected function getMockRepositoryRegistry($statement = null): Registry
    {
        if ($statement === null) {
            $statement = $this->createMock(Statement::class);
            $statement->method('bindValue')
                ->willReturn($statement);
        }

        $connection = $this->createMock(Connection::class);
        $connection->method('executeQuery')
            ->willReturn($statement);

        $connection->method('prepare')
            ->willReturn($statement);

        $classMetaData = $this->createMock(ClassMetadata::class);

        $cache = $this->createMock(Cache::class);
        $cache->method('delete')
            ->willReturn(true);

        $configuration = $this->createMock(Configuration::class);
        $configuration->method('getResultCacheImpl')
            ->willReturn($cache);

        $expr = $this->createMock(Expr::class);

        $queryBuilder = $this->createMock(QueryBuilder::class);
        $queryBuilder->method('expr')
            ->willReturn($expr);

        $queryBuilder->method('select')
            ->willReturn($queryBuilder);

        $queryBuilder->method('from')
            ->willReturn($queryBuilder);

        $queryBuilder->method('where')
            ->willReturn($queryBuilder);

        $queryBuilder->method('andWhere')
            ->willReturn($queryBuilder);

        $queryBuilder->method('orderBy')
            ->willReturn($queryBuilder);

        $queryBuilder->method('join')
            ->willReturn($queryBuilder);

        $queryBuilder->method('setParameter')
            ->willReturn($queryBuilder);

        $queryBuilder->method('setMaxResults')
            ->willReturn($queryBuilder);

        $queryBuilder->method('where')
            ->willReturn($queryBuilder);

        $queryBuilder->method('leftJoin')
            ->willReturn($queryBuilder);

        $entityManager = $this->createMock(EntityManager::class);

        $entityManager->method('getConnection')
            ->willReturn($connection);

        $entityManager->method('getClassMetadata')
            ->willReturn($classMetaData);

        $entityManager->method('getConfiguration')
            ->willReturn($configuration);

        $entityManager->method('createQueryBuilder')
            ->willReturn($queryBuilder);

        $query = $this->getMockBuilder(AbstractQuery::class)
            ->setConstructorArgs([$entityManager])
            ->setMethods(['useQueryCache', 'getSQL', '_doExecute', 'setParameter', 'getOneOrNullResult', 'setMaxResults', 'getResult', 'getArrayResult', 'getSingleScalarResult'])
            ->getMock();

        $query->method('setParameter')
            ->willReturn($query);

        $query->method('useQueryCache')
            ->willReturn($query);

        $query->method('setMaxResults')
            ->willReturn($query);

        $entityManager->method('createQuery')
            ->willReturn($query);

        $queryBuilder->method('getQuery')
            ->willReturn($query);

        $entityManager->method('createNativeQuery')
            ->willReturn($query);

        $registry = $this->createMock(Registry::class);
        $registry->method('getManagerForClass')
            ->willReturn($entityManager);

        /** @var Registry $registry */
        return $registry;
    }

    /**
     * @return Application
     */
    public function getApplication()
    {
        if ($this->application === null) {
            $kernel = self::bootKernel();
            $this->application = new Application($kernel);
        }
        return $this->application;
    }
}
