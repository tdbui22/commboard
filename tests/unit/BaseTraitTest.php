<?php

namespace App\Tests\Unit;

use App\BaseTrait;
use App\Tests\AppTest;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

class BaseTraitTest extends AppTest
{
    public function testView()
    {
        $twig = $this->createMock(\Twig_Environment::class);
        $twig->method('render')
            ->willReturn(__METHOD__);

        /** @var \Twig_Environment $twig */
        $baseTrait = new BaseBaseTrait();
        $result = $baseTrait->testView($twig, 'view');
        $this->assertInstanceOf(Response::class, $result);
        $this->assertSame(__METHOD__, $result->getContent());
    }

    public function testRedirectRoute()
    {
        $url = 'https://www.example.com';
        $router = $this->createMock(RouterInterface::class);
        $router->method('generate')
            ->willReturn($url);

        /** @var RouterInterface $router */

        $baseTrait = new BaseBaseTrait();
        $result = $baseTrait->testRedirectRoute($router, 'name');
        $this->assertInstanceOf(RedirectResponse::class, $result);
        $this->assertSame($url, $result->getTargetUrl());
    }

    public function testIsProd()
    {
        $baseTrait = new BaseBaseTrait();
        $this->assertFalse($baseTrait->testIsProd());
    }

    public function testEnv()
    {
        $baseTrait = new BaseBaseTrait();
        $this->assertSame('test', $baseTrait->testEnv());
    }

    public function testRunTime()
    {
        $trait = new BaseBaseTrait();
        $this->assertGreaterThan(time(), $trait->runTime(30));

        $trait->testMode();
        $this->assertGreaterThan(time(), $trait->runTime(30));
    }

    public function testSleep()
    {
        $trait = new BaseBaseTrait();
        $trait->testMode()->sleep(30);
        $this->assertTrue(true);
    }
}

class BaseBaseTrait
{
    use BaseTrait;

    public function testView(\Twig_Environment $twig, $view, $parameters = [], Response $response = null)
    {
        return $this->view($twig, $view, $parameters, $response);
    }

    public function testRedirectRoute(RouterInterface $router, $name, $parameters = [], $status = 302)
    {
        return $this->redirectRoute($router, $name, $parameters, $status);
    }

    public function testIsProd()
    {
        return $this->isProd();
    }

    public function testEnv()
    {
        return $this->env();
    }
}