#!/usr/bin/env bash

cd "${BASH_SOURCE%/*}" || exit

TESTDOX=''

while getopts ":t" opt; do
    case ${opt} in
        t ) TESTDOX='--testdox'
        ;;
  esac
done

APP_ENV=test ./clear_cache.sh
APP_ENV=test ./clear_memory.sh

php bin/phpunit --coverage-html coverage ${TESTDOX} --testsuite unit
