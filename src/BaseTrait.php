<?php

namespace App;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\RouterInterface;

trait BaseTrait
{
    private $testMode = false;

    private $testAsProd = false;

    /**
     * @param \Twig_Environment $twig
     * @param $view
     * @param array $parameters
     * @param Response|null $response
     * @return Response
     * @throws \Twig_Error_Loader
     * @throws \Twig_Error_Runtime
     * @throws \Twig_Error_Syntax
     */
    protected function view(\Twig_Environment $twig, $view, $parameters = [], Response $response = null)
    {
        if ($response === null) {
            $response = new Response();
        }

        return $response->setContent($twig->render($view, $parameters));
    }

    protected function viewOnly(\Twig_Environment $twig, $view, $parameters = [])
    {
        return $twig->render($view, $parameters);
    }

    protected function redirectRoute(RouterInterface $router, $name, $parameters = [], $status = 302)
    {
        return new RedirectResponse($router->generate($name, $parameters), $status);
    }

    protected function isProd()
    {
        if ($this->testMode && $this->testAsProd) {
            return true;
        }

        return getenv('APP_ENV') == 'prod';
    }

    protected function env()
    {
        return getenv('APP_ENV');
    }

    public function testMode($bool = true, $asProd = false)
    {
        $this->testMode = $bool;
        $this->testAsProd = $asProd;
        return $this;
    }

    public function runTime($duration)
    {
        return $this->testMode ? time() + 1 : time() + $duration;
    }

    public function sleep($i)
    {
        $sleep = $this->testMode ? 0 : 1;
        sleep($sleep * $i);
    }

    protected function insertFlash(Session $session, string $type, string $message)
    {
        $session->getFlashBag()->add($type, $message);
    }
}
