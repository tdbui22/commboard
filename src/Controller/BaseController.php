<?php

namespace App\Controller;

use App\BaseTrait;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

abstract class BaseController extends AbstractController
{
    use BaseTrait;
}
