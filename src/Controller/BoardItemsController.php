<?php

namespace App\Controller;

use App\Repository\ApplicantRepository;
use App\Repository\ItemRepository;

class BoardItemsController extends BaseController
{
    public function action($applicantPubId, $categoryId, \Twig_Environment $twig, ApplicantRepository $applicantRepository, ItemRepository $itemRepository)
    {
        $applicant = $applicantRepository->query($applicantPubId)
            ->noCache()
            ->fetchOne();

        $items = $itemRepository
            ->noCache()
            ->queryByCategory($categoryId)
            ->fetchAll();

        return $this->view($twig, 'board/items.html.twig', [
            'applicant' => $applicant,
            'items' => $items,
        ]);
    }
}
