<?php

namespace App\Controller;

use App\Repository\ApplicantRepository;
use App\Repository\CategoryRepository;

class BoardCategoriesController extends BaseController
{
    public function action($applicantPubId, \Twig_Environment $twig, ApplicantRepository $applicantRepository, CategoryRepository $categoryRepository)
    {
        $applicant = $applicantRepository->query($applicantPubId)
            ->noCache()
            ->fetchOne();

        $categories = $categoryRepository
            ->noCache()
            ->queryAll()
            ->fetchAll();

        return $this->view($twig, 'board/categories.html.twig', [
            'applicant' => $applicant,
            'categories' => $categories,
        ]);
    }
}
