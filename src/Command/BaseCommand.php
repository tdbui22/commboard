<?php

namespace App\Command;

use App\BaseTrait;
use Symfony\Component\Console\Command\Command;

abstract class BaseCommand extends Command
{
    use BaseTrait;
}
