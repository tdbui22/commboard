<?php

namespace App\Repository;

use App\Entity\Applicant;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ApplicantRepository extends BaseRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Applicant::class);
    }

    public function query($publicId)
    {
        $dql = <<<DQL
SELECT a
FROM App:Applicant a
WHERE a.publicId = :publicId
DQL;

        $query = $this->getEntityManager()->createQuery($dql)
            ->setParameter('publicId', $publicId);

        return $this->registerQuery($query);
    }
}
