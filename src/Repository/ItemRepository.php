<?php

namespace App\Repository;

use App\Entity\Item;
use Symfony\Bridge\Doctrine\RegistryInterface;

class ItemRepository extends BaseRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Item::class);
    }

    public function queryByCategory($categoryId)
    {
        $dql = <<<DQL
SELECT i
FROM App:Item i
WHERE i.category = :categoryId
DQL;

        $query = $this->getEntityManager()->createQuery($dql)
            ->setParameter('categoryId', $categoryId);
        
        return $this->registerQuery($query);
    }
}
