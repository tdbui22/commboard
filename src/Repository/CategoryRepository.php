<?php

namespace App\Repository;

use App\Entity\Category;
use Symfony\Bridge\Doctrine\RegistryInterface;

class CategoryRepository extends BaseRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Category::class);
    }

    public function queryAll()
    {
        $dql = <<<DQL
SELECT c
FROM App:Category c
DQL;

        $query = $this->getEntityManager()->createQuery($dql);
        
        return $this->registerQuery($query);
    }
}
