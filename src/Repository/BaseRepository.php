<?php

namespace App\Repository;

use App\Component\Cache\CacheManager;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\Query;

abstract class BaseRepository extends ServiceEntityRepository
{
    private $deleteCache = false;

    private $useCache = true;

    /** @var Query */
    private $query;

    public function clearCache()
    {
        $this->deleteCache = true;
        return $this;
    }

    public function noCache()
    {
        $this->useCache = false;
        return $this;
    }

    protected function registerQuery(AbstractQuery $query, $cacheTime = 3600)
    {
        $this->query = $query;
        $this->query->useQueryCache(true);

        $debug = debug_backtrace(DEBUG_BACKTRACE_PROVIDE_OBJECT, 2);
        $cacheKey = CacheManager::cacheKey($debug[1]['class'], $debug[1]['function'], $debug[1]['args']);

        if ($this->deleteCache === true) {
            $this->deleteCacheInternal($cacheKey);
        }
        $this->deleteCache = false;

        if ($this->useCache === true) {
            $this->query->useResultCache(true, $cacheTime, $cacheKey);
        }
        $this->useCache = true;

        return $this;
    }

    public function deleteCache($method, array $args = [])
    {
        $cacheKey = CacheManager::cacheKey(get_called_class(), $method, $args);
        return $this->deleteCacheInternal($cacheKey);
    }

    private function deleteCacheInternal($cacheKey)
    {
        return $this->getEntityManager()->getConfiguration()->getResultCacheImpl()->delete($cacheKey);
    }

    public function fetchAll()
    {
        $result = $this->query->getResult();
        if (empty($result)) {
            return [];
        }
        return $result;
    }

    public function fetchOne()
    {
        return $this->query->getOneOrNullResult();
    }

    public function fetchAllArray()
    {
        $result = $this->query->getArrayResult();
        if (empty($result)) {
            return [];
        }
        return $result;
    }

    public function fetchOneArray()
    {
        $result = $this->query->getArrayResult();
        if (empty($result)) {
            return null;
        }
        return $result[0];
    }
}
