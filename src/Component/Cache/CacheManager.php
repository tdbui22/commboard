<?php

namespace App\Component\Cache;

class CacheManager
{
    public static function cacheKey($class, $method, $keys)
    {
        $a = $class . '~' . $method . '~' . var_export($keys, true);
        return preg_replace('/(\{|\}|\(|\)|\/|\\\|\@|\:)/', '~', $a);
//        return md5($class . '::' . $method . '::' . var_export($keys, true));
    }
}
