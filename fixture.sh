#!/usr/bin/env bash

cd "${BASH_SOURCE%/*}" || exit

OPT=$1

if [ "$1" == 0 ]; then
    ./setup/db/init.sh
    OPT=2
elif [ "$OPT" == '' ]; then
    OPT=1
fi

for ((i = 1; i <= $OPT; i++)); do
    bin/phpunit --filter testMock tests/fixture/FixtureTest.php
done

if [ "$1" != 0 ]; then
    ./clear_memory.sh
fi

bin/console spotlight:update
