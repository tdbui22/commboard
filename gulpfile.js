'use strict';

var gulp = require('gulp');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

gulp.task('sass', function () {
    return gulp.src('./assets/sass/**/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('./public/build/css/'));
});

gulp.task('sass:watch', function () {
    gulp.watch(['./assets/sass/**/*.scss', './assets/sass/*.scss'], ['sass']);
});
